// BÀI1
document.getElementById("btn-hom-qua").onclick = function () {
  var dateValue = document.getElementById("txt-date").value;
  console.log("Hôm nay: ", dateValue);

  var ketqua1 = document.getElementById("ket-qua-5");
  var homNay = new Date(dateValue);
  var homqua = 0;
  homqua = new Date(
    homNay.getFullYear(),
    homNay.getMonth(),
    homNay.getDate() - 1
  );
  console.log("Hôm qua: ", homqua);

  ketqua1.innerHTML = `Hôm qua: ${homqua}`;
};

document.getElementById("btn-ngay-mai").onclick = function () {
  var dateValue = document.getElementById("txt-date").value;
  console.log("Hôm nay: ", dateValue);

  var ketqua1 = document.getElementById("ket-qua-5");
  var homNay = new Date(dateValue);
  var ngayMai = 0;
  ngayMai = new Date(
    homNay.getFullYear(),
    homNay.getMonth(),
    homNay.getDate() + 1
  );
  console.log("Ngày mai: ", ngayMai);
  ketqua1.innerHTML = `Ngày mai: ${ngayMai}`;
};

// BÀI 2
document.getElementById("btn-tinh-ngay").onclick = function () {
  var thangValue = document.getElementById("txt-month").value * 1;
  var namValue = document.getElementById("txt-year").value * 1;
  var ketQua6 = document.getElementById("ket-qua-6");

  switch (thangValue) {
    case 4:
    case 6:
    case 9:
    case 11:
      ketQua6.innerHTML = `Tháng ${thangValue} năm ${namValue} có 30 ngày`;
      break;
    case 2:
      if (
        (namValue % 4 === 0 && namValue % 100 !== 0) ||
        namValue % 400 === 0
      ) {
        ketQua6.innerHTML = `Tháng ${thangValue} năm ${namValue} có 29 ngày`;
      } else {
        ketQua6.innerHTML = `Tháng ${thangValue} năm ${namValue} có 28 ngày`;
      }
      break;
    default:
      ketQua6.innerHTML = `Tháng ${thangValue} năm ${namValue} có 31 ngày`;
  }
  return false;
};

// BÀI 7
function docSo(iSo) {
  var chuSo = new Array(
    " không ",
    " một ",
    " hai ",
    " ba ",
    " bốn ",
    " năm ",
    " sáu ",
    " bảy ",
    " tám ",
    " chín "
  );

  var hangDonVi, hangChuc, hangTram;
  var result = "";
  hangTram = parseInt(iSo / 100);
  hangChuc = parseInt((iSo % 100) / 10);
  hangDonVi = iSo % 10;
  if (hangTram == 0 && hangChuc == 0 && hangDonVi == 0) {
    return "";
  }
  if (hangTram != 0) {
    result += chuSo[hangTram] + " trăm";
    if (hangChuc == 0 && hangDonVi != 0) {
      result += " linh";
    }
  }

  if (hangChuc != 0 && hangChuc != 1) {
    result += chuSo[hangChuc] + "mươi";
    if (hangChuc == 0 && hangDonVi != 0) {
      result = result + " linh";
    }
  }

  if (hangChuc == 1) {
    result += " mười";
  }
  switch (hangDonVi) {
    case 1:
      if (hangChuc != 0 && hangChuc != 1) {
        result += " mốt ";
      } else {
        result += chuSo[hangDonVi];
      }
      break;
    case 5:
      if (hangChuc == 0) {
        result += chuSo[hangDonVi];
      } else {
        result += " lăm ";
      }
      break;
    default:
      if (hangDonVi != 0) {
        result += chuSo[hangDonVi];
      }
      break;
  }
  return result;
}

document.getElementById("btn-doc-so").onclick = function () {
  var soValue = document.getElementById("so-3-chu-so").value;
  var ketQua7 = document.getElementById("ket-qua-7");
  ketQua7.innerHTML = docSo(soValue);
};

// BÀI 8
function tinhKhoangCach(x1, y1, x2, y2) {
  return (khoangCach = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
}

document.getElementById("btn-toa-do").onclick = function () {
  var tenSv1 = document.getElementById("txt-sv1").value;
  var toaDoXCuaSv1 = document.getElementById("txt-toa-do-sv1-x").value * 1;
  var toaDoYCuaSv1 = document.getElementById("txt-toa-do-sv1-y").value * 1;

  var tenSv2 = document.getElementById("txt-sv2").value;
  var toaDoXCuaSv2 = document.getElementById("txt-toa-do-sv2-x").value * 1;
  var toaDoYCuaSv2 = document.getElementById("txt-toa-do-sv2-y").value * 1;

  var tenSv3 = document.getElementById("txt-sv3").value;
  var toaDoXCuaSv3 = document.getElementById("txt-toa-do-sv3-x").value * 1;
  var toaDoYCuaSv3 = document.getElementById("txt-toa-do-sv3-y").value * 1;

  var toaDoXCuaTruong = Number(
    document.getElementById("txt-toa-do-truong-x").value * 1
  );
  var toaDoYCuaTruong =
    document.getElementById("txt-toa-do-truong-y").value * 1;

  var khoangCachSv1DenTruong = tinhKhoangCach(
    toaDoXCuaSv1,
    toaDoYCuaSv1,
    toaDoXCuaTruong,
    toaDoYCuaTruong
  );
  console.log("khoangCachSv1DenTruong: ", khoangCachSv1DenTruong);
  var khoangCachSv2DenTruong = tinhKhoangCach(
    toaDoXCuaSv2,
    toaDoYCuaSv2,
    toaDoXCuaTruong,
    toaDoYCuaTruong
  );
  console.log("khoangCachSv2DenTruong: ", khoangCachSv2DenTruong);
  var khoangCachSv3DenTruong = tinhKhoangCach(
    toaDoXCuaSv3,
    toaDoYCuaSv3,
    toaDoXCuaTruong,
    toaDoYCuaTruong
  );
  console.log("khoangCachSv3DenTruong: ", khoangCachSv3DenTruong);

  var ketQua8 = document.getElementById("ket-qua-8");

  if (
    khoangCachSv1DenTruong > khoangCachSv2DenTruong &&
    khoangCachSv1DenTruong > khoangCachSv3DenTruong
  ) {
    ketQua8.innerHTML = `Sinh viên xa trường nhất là: ${tenSv1}`;
  } else if (
    khoangCachSv2DenTruong > khoangCachSv1DenTruong &&
    khoangCachSv2DenTruong > khoangCachSv3DenTruong
  ) {
    ketQua8.innerHTML = `Sinh viên xa trường nhất là: ${tenSv2}`;
  } else {
    ketQua8.innerHTML = `Sinh viên xa trường nhất là: ${tenSv3}`;
  }
};
